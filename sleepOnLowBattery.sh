#!/bin/sh

sleepOnLowBattery () {
	while true; do
		# change BAT0 to whatever your battery is identified as. Typically BAT0 or BAT1
		charge="$(cat /sys/class/power_supply/BAT0/capacity)"
		status="$(cat /sys/class/power_supply/BAT0/status)"

		if [ -n "${charge}" ] && [ -n "${status}" ]; then
			case "${status}" in
			Charging) status='+';;
			Discharging) status='-';;
			Full) status='=';;
			Unknown) status='?';;
			esac
			echo "${charge}${status}"
			if [ "${charge}" -le "${1:-10}" ] && { [ "${status}" != '+' ] && [ "${status}" != '=' ]; }; then
				echo "suspending"
				slock & systemctl suspend
			else
				echo "not suspending"
			fi
		else
			echo "not enough data"
		fi
		sleep "${2:-60}"
	done
}

sleepOnLowBattery "${@}"
